<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>个人简历</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <style>

        li {
            list-style-type: none;
            margin-bottom: 10px; /* 调整每个li之间的间距 */
        }

        body {
            font-family: '微软雅黑', sans-serif;
            line-height: 1.6;
            color: #333;
            margin: 0;
            padding: 0;
        }

        header {
            display: flex;
            align-items: center;
            padding: 2em 20px;
            animation: fadeIn 1s ease-in-out;
        }

        h1 {
            font-size: 3em;
            margin: 0;
            color: #3498db;
        }

        p {
            color: #3498db;
            margin: 0;
        }

        h2 {
            font-size: 2em;
            margin-bottom: 20px;
            color: #333;
            position: relative;
            display: inline-block;
        }

        h2::before,
        h2::after {
            content: '';
            position: absolute;
            width: 50%;
            height: 2px;
            background-color: #3498db;
            top: 100%;
            transform: translateY(-50%);
        }

        h2::before {
            left: 0;
            margin-right: 10px;
        }

        h2::after {
            right: 0;
            margin-left: 10px;
        }

        ul {
            list-style: none;
            padding: 0;
        }

        li {
            margin-bottom: 10px;
        }

        section {
            max-width: 800px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
            transition: transform 0.3s ease-in-out, opacity 0.5s ease-in-out;
            animation: fadeInAndScale 0.5s ease-in-out;
        }

        section:hover {
            transform: scale(1.02);
        }

        .project-item {
            border-bottom: 1px solid #ddd;
            padding-bottom: 15px;
            margin-bottom: 15px;
        }

        .icon {
            color: #3498db;
            margin-right: 5px;
        }

        @keyframes fadeInAndScale {
            from {
                opacity: 0;
                transform: translateY(-20px);
            }
            to {
                opacity: 1;
                transform: translateY(0);
            }
        }

        @keyframes gradientAnimation {
            0% {
                background-position: 0% 50%;
            }
            50% {
                background-position: 100% 50%;
            }
            100% {
                background-position: 0% 50%;
            }
        }

        body {
            animation: gradientAnimation 15s infinite linear;
            background: linear-gradient(45deg, #3498db, #ecf0f1, #3498db);
            background-size: 400% 400%;
        }

        a {
            color: #3498db;
            text-decoration: none;
        }

        hr {
            margin: 20px 0;
            border: 0;
            height: 1px;
            background: linear-gradient(to right, transparent, #3498db, transparent);
        }

        .fancy-divider {
            text-align: center;
            margin: 20px 0;
            position: relative;
        }

        .fancy-divider::before {
            content: '';
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            height: 1px;
            background: linear-gradient(to right, transparent, #3498db, transparent);
        }

        .fancy-divider span {
            background: #fff;
            padding: 5px 15px;
            font-size: 1.2em;
            position: relative;
            z-index: 1;
        }

        .profile-picture {
            width: 150px;
            height: 150px;
            border-radius: 50%;
            overflow: hidden;
        }

        .profile-picture img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        @media screen and (max-width: 600px) {
            h1 {
                font-size: 2.5em;
            }

            h2 {
                font-size: 1.8em;
            }
        }

        .li-work {
            float: right;
            color: #3498db;
        }

        .li-project span {
            color: #3498db;
        }

    </style>
    <!-- PC端样式 -->
    <style>
        .pc-style {
            display: block;
        }

        .mobile-style {
            display: none;
        }
    </style>

    <!-- 移动端样式 -->
    <style>
        @media only screen and (max-width: 600px) {
            .pc-style {
                display: none;
            }

            .mobile-style {
                display: block;
            }
        }
    </style>
</head>

<body>

<!-- PC端逻辑 -->
<div class="pc-style">
    <header style=" position: fixed; margin-left: 220px;">
        <div>
            <h1>${user.name}</h1>
            <p style="color: #3498db;">
                <span> 求职意向 &nbsp;&nbsp;：<span>${user.like}</span></span><br>
                <span> Gitee仓库 ：<span><a href="https://gitee.com/jiashenliu/projects">gitee.com</a></span></span><br>
                <span> CSDN博客：<span><a
                                href="https://blog.csdn.net/qq_42926439?spm=1000.2115.3001.5343">blog.csdn.net</a></span></span>
            </p>
        </div>

    </header>
</div>

<!-- 移动端逻辑 -->
<div class="mobile-style">
    <header>
        <div>
            <h1>${user.name}</h1>
            <p style="color: #3498db;">
                <span> 求职意向 &nbsp;&nbsp;：<span>${user.like}</span></span><br>
                <span> Gitee仓库 ：<span><a href="https://gitee.com/jiashenliu/projects">gitee.com</a></span></span><br>
                <span> CSDN博客：<span><a
                                href="https://blog.csdn.net/qq_42926439?spm=1000.2115.3001.5343">blog.csdn.net</a></span></span>
            </p>
        </div>

    </header>
</div>


<!-- 个人信息 -->
<section>
    <h2>个人信息</h2>
    <ul>
        <li><i class="icon fas fa-male"></i> 性别： ${user.gender}</li>
        <li><i class="icon fas fa-birthday-cake"></i> 年龄：${user.age}岁</li>
        <li><i class="icon fas fa-phone"></i> 电话：${user.phone}</li>
        <li><i class="icon far fa-envelope"></i> 邮箱：${user.email}</li>
    </ul>
</section>

<#--专业技能-->
<section>
    <h2>专业技能</h2>

    <#list advantageList as item>
        <li><i class="icon fas fa-regular fa-file"></i>${item.advantageDescription}</li>
    </#list>
</section>

<#--工作经验-->
<section>
    <h2>工作经验</h2>
    <#list 0..(worklist?size - 1) as index>
        <#assign work = worklist[index]>
        <li>
            <i class="icon fas fa-regular fa-briefcase"></i>
            <strong style="color: #3498db;">${work.companyName} &nbsp;&nbsp; | &nbsp;&nbsp; ${work.position}</strong>
            <span class="li-work">
                 ${work.startTime}
            - ${work.endTime}
            </span>
        </li>

        <#list jobAssignmentsMap["jobAssignments" + (index + 1)] as assignment>
            <#assign iconClass = "icon fas fa-regular fa-" + (assignment?index + 1)>
            <li><i class="${iconClass}">、</i>${assignment.taskDescription}</li>
        </#list>
        <hr/>
    </#list>
</section>

<!-- 项目经历 -->
<section>
    <h2>项目经验</h2>
    <#list projectExperiencesMap?keys as projectKey>
        <div>
            <i class="icon fas fa-regular fa-briefcase"></i>
            <!-- 显示项目经历 -->
            <#list projectExperiencesMap[projectKey] as project>
                <strong style="color: #3498db;">${project.projectName} &nbsp;&nbsp; |
                    &nbsp;&nbsp; ${project.workExperience.companyName}</strong>
                <span class="li-work"> ${project.startTime} - ${project.endTime} </span>
                <li class="li-project"><span
                            class="icon fas fa-regular fa-1">、项目描述</span>：${project.projectDescription?default('')}
                </li>
                <li class="li-project"><span
                            class="icon fas fa-regular fa-2">、技术栈：</span> ${project.techStack?default('')}</li>
                <li class="li-project"><span
                            class="icon fas fa-regular fa-3">、工作内容：</span>${project.workContent?default('')}</li>
                <#if project.achievements?has_content>
                    <li class="li-project"><span class="icon fas fa-regular fa-4">、业绩：</span>${project.achievements}
                    </li>
                </#if>
                <hr/>
            </#list>
        </div>
    </#list>
</section>


<#--教育经历-->
<section>
    <h2>教育经历</h2>
    <ul>
        <li><i class="icon fas fa-university"></i> 学校：${school.schoolName}</li>
        <li><i class="icon fas fa-graduation-cap"></i> 学历：${school.degree}</li>
        <li><i class="icon fas fa-code"></i> 专业：${school.specialized}</li>
    </ul>
</section>

<hr>
</body>
</html>
