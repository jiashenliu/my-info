package cn.ljs.myinfo.controller;

import cn.ljs.myinfo.entity.*;
import cn.ljs.myinfo.service.impl.CommonServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@Slf4j
public class MyInfoController {

    @Autowired
    CommonServiceImpl common;

    @RequestMapping("/show")
    public String myJava(Model model) {

//用户信息
        User user = common.getUserInfo(0);
        model.addAttribute("user", user);
//个人优势
        List<UserAdvantage> advantageList = common.getUserAdvantage(user);
        model.addAttribute("advantageList", advantageList);
//工作经历
        List<WorkExperience> worklist = common.getWorkExperience(user);
        model.addAttribute("worklist", worklist);

//具体安排
        Map<String, List<JobAssignment>> jobAssignmentsMap = common.getJobAssignment(worklist);
        model.addAttribute("jobAssignmentsMap", jobAssignmentsMap);

//项目经历
        Map<String, List<ProjectExperience>> projectExperiencesMap = common.getProjectExperience(worklist);
        model.addAttribute("projectExperiencesMap", projectExperiencesMap);

//学校
        School school = common.getSchool();
        model.addAttribute("school", school);

        return "info";
    }

}
