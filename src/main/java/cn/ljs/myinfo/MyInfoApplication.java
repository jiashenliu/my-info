package cn.ljs.myinfo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.ljs.myinfo.mapper")
public class MyInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyInfoApplication.class, args);
    }

}
