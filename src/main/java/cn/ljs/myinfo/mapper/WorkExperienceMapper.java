package cn.ljs.myinfo.mapper;

import cn.ljs.myinfo.entity.WorkExperience;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
public interface WorkExperienceMapper extends BaseMapper<WorkExperience> {

}
