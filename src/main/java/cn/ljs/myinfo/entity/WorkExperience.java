package cn.ljs.myinfo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.time.LocalDate;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javafx.scene.input.DataFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 *
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("work_experience")
public class WorkExperience implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer userId;

    private String companyName;

    private String position;


    private String startTime;

    private String endTime;

    public String getStartTime() {
        return startTime.substring(0, 7);
    }

    public String getEndTime() {
        return endTime.substring(0, 7);
    }
}
