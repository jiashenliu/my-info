package cn.ljs.myinfo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;


import java.beans.Transient;
import java.time.LocalDate;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("project_experience")
public class ProjectExperience implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer experienceId;

    private String projectName;

    private String projectDescription;

    private String techStack;

    private String workContent;

    private String achievements;

    private String startTime;

    private String endTime;

    @TableField(exist = false)
    private WorkExperience workExperience;

    public String getStartTime() {
        return startTime.substring(0, 7);
    }

    public String getEndTime() {
        return endTime.substring(0, 7);
    }
}
