package cn.ljs.myinfo.service;

import cn.ljs.myinfo.entity.JobAssignment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
public interface IJobAssignmentService extends IService<JobAssignment> {

}
