package cn.ljs.myinfo.service;

import cn.ljs.myinfo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
public interface IUserService extends IService<User> {

}
