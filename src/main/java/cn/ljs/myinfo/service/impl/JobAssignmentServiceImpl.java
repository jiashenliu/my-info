package cn.ljs.myinfo.service.impl;

import cn.ljs.myinfo.entity.JobAssignment;
import cn.ljs.myinfo.mapper.JobAssignmentMapper;
import cn.ljs.myinfo.service.IJobAssignmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Service
public class JobAssignmentServiceImpl extends ServiceImpl<JobAssignmentMapper, JobAssignment> implements IJobAssignmentService {

}
