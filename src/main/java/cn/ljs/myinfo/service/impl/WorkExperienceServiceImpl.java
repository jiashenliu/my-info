package cn.ljs.myinfo.service.impl;

import cn.ljs.myinfo.entity.WorkExperience;
import cn.ljs.myinfo.mapper.WorkExperienceMapper;
import cn.ljs.myinfo.service.IWorkExperienceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Service
public class WorkExperienceServiceImpl extends ServiceImpl<WorkExperienceMapper, WorkExperience> implements IWorkExperienceService {

}
