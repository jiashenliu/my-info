package cn.ljs.myinfo.service.impl;

import cn.ljs.myinfo.entity.User;
import cn.ljs.myinfo.mapper.UserMapper;
import cn.ljs.myinfo.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
