package cn.ljs.myinfo.service.impl;

import cn.ljs.myinfo.entity.ProjectExperience;
import cn.ljs.myinfo.mapper.ProjectExperienceMapper;
import cn.ljs.myinfo.service.IProjectExperienceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Service
public class ProjectExperienceServiceImpl extends ServiceImpl<ProjectExperienceMapper, ProjectExperience> implements IProjectExperienceService {

}
