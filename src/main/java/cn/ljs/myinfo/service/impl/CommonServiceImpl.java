package cn.ljs.myinfo.service.impl;

import cn.ljs.myinfo.entity.*;
import cn.ljs.myinfo.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class CommonServiceImpl {
    private final IJobAssignmentService jobAssignmentService;
    private final IProjectExperienceService projectExperienceService;
    private final ISchoolService schoolService;
    private final IUserAdvantageService userAdvantageService;
    private final IUserService userService;
    private final IWorkExperienceService workExperienceService;

    //基本信息
    public User getUserInfo(Integer id) {
        User user = userService.getOne(new QueryWrapper<User>().eq("likes", id));
        Integer now = Integer.valueOf(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy")));
        Integer birthday = Integer.valueOf(user.getBirthday().toString().substring(0, 4));
        return user.setAge(now - birthday);
    }

    //个人优势
    public List<UserAdvantage> getUserAdvantage(User user) {
        return userAdvantageService.list(new QueryWrapper<UserAdvantage>().eq("user_id", user.getId()));
    }

    //工作经历
    public List<WorkExperience> getWorkExperience(User user) {
        List<WorkExperience> worklist = workExperienceService.list(new QueryWrapper<WorkExperience>().eq("user_id", user.getId()));
        for (WorkExperience workExperience : worklist) {
            workExperience.setStartTime(workExperience.getStartTime());
            workExperience.setEndTime(workExperience.getEndTime());
        }
        return worklist;
    }

    //项目经历
    public Map<String, List<ProjectExperience>> getProjectExperience(List<WorkExperience> worklist) {
        int count = worklist.size() - 1;
        Map<String, List<JobAssignment>> jobAssignmentsMap = new HashMap<>();
        Map<String, List<ProjectExperience>> projectExperiencesMap = new HashMap<>();

        while (count >= 0) {
//具体安排
            List<JobAssignment> experienceId = jobAssignmentService.list(new QueryWrapper<JobAssignment>().eq("experience_id", worklist.get(count).getId()));
            jobAssignmentsMap.put("jobAssignments" + String.valueOf(count + 1), experienceId);
//项目经历
            List<ProjectExperience> projectExperience = projectExperienceService.list(new QueryWrapper<ProjectExperience>().eq("experience_id", worklist.get(count).getId()));
            for (ProjectExperience experience : projectExperience) {
                //查询出工作经历中的公司信息
                WorkExperience workExperience = workExperienceService.getOne(new QueryWrapper<WorkExperience>().eq("id", experience.getExperienceId()));
                experience.setWorkExperience(workExperience);
                experience.setStartTime(experience.getStartTime());
                experience.setEndTime(experience.getEndTime());
            }
            projectExperiencesMap.put("projectExperiences" + String.valueOf(count + 1), projectExperience);
            count--;
        }
        return projectExperiencesMap;
    }

    //工作经历
    public Map<String, List<JobAssignment>> getJobAssignment(List<WorkExperience> worklist) {
        int count = worklist.size() - 1;
        Map<String, List<JobAssignment>> jobAssignmentsMap = new HashMap<>();
        while (count >= 0) {
            List<JobAssignment> experienceId = jobAssignmentService.list(new QueryWrapper<JobAssignment>().eq("experience_id", worklist.get(count).getId()));
            jobAssignmentsMap.put("jobAssignments" + String.valueOf(count + 1), experienceId);
            count--;
        }
        return jobAssignmentsMap;
    }

    //学校信息
    public School getSchool() {
        return schoolService.getOne(null);
    }

}
