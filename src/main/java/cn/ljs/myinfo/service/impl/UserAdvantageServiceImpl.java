package cn.ljs.myinfo.service.impl;

import cn.ljs.myinfo.entity.UserAdvantage;
import cn.ljs.myinfo.mapper.UserAdvantageMapper;
import cn.ljs.myinfo.service.IUserAdvantageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Service
public class UserAdvantageServiceImpl extends ServiceImpl<UserAdvantageMapper, UserAdvantage> implements IUserAdvantageService {

}
