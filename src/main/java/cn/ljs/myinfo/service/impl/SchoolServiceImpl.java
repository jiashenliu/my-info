package cn.ljs.myinfo.service.impl;

import cn.ljs.myinfo.entity.School;
import cn.ljs.myinfo.mapper.SchoolMapper;
import cn.ljs.myinfo.service.ISchoolService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2024-02-24
 */
@Service
public class SchoolServiceImpl extends ServiceImpl<SchoolMapper, School> implements ISchoolService {

}
