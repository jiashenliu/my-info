INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (1, 1, '负责公司各地区系统及数据接口的扩展');
INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (6, 1, '负责各地区项目突发情况处理');
INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (7, 1, '修复程序及接口 Bug 保证客户正常使用');
INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (8, 1, '参与公司组织的业务扩展培训');
INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (9, 1, '完成上级交办的其他事宜');
INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (10, 2, '根据项目组长提供文档完成开发任务');
INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (11, 2, '负责领导交代的事宜');
INSERT INTO `job_assignment` (`id`, `experience_id`, `task_description`) VALUES (12, 2, '参加公司组织培训技术分析会议');
